package com.trash.contactosapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class ContactoActivity extends AppCompatActivity {

    TextView tVNombre, tVApellido, tVTelefono, tVEmail;
    CardView btnLlamar, btnSalir;

    private final int PHONE_CALL_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacto);

        Bundle bundle = getIntent().getExtras();
        String nombre = bundle.getString("NOMBRECONTACTO");
        String apellido = bundle.getString("APELLIDOCONTACTO");
        String telefono = bundle.getString("TELEFONOCONTACTO");
        String email = bundle.getString("EMAILCONTACTO");

        tVNombre = (TextView) findViewById(R.id.lblNombre);
        tVApellido = (TextView) findViewById(R.id.lblApellido);
        tVTelefono = (TextView) findViewById(R.id.lblTelefono);
        tVEmail = (TextView) findViewById(R.id.lblEmail);
        btnLlamar = (CardView) findViewById(R.id.btnCall);
        btnSalir = (CardView) findViewById(R.id.btnLogOut);

        tVNombre.setText(nombre);
        tVApellido.setText(apellido);
        tVTelefono.setText(telefono);
        tVEmail.setText(email);

        btnLlamar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String numeroTel = tVTelefono.getText().toString();
                if (numeroTel != null){
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, PHONE_CALL_CODE);
                        //Toast.makeText(ContactoActivity.this, "Llamando a: " + numeroTel, Toast.LENGTH_SHORT).show();
                    } else {
                        viejoSDK(numeroTel);
                    }
                }
            }
            private  void viejoSDK (String numeroTel){
                Intent llamar = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + numeroTel.toString()));
                if (revPermisos(Manifest.permission.CALL_PHONE)){
                    startActivity(llamar);
                } else {
                    Toast.makeText(ContactoActivity.this, "Falta activar el permiso de llamadas", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cerrarSesion = new Intent(ContactoActivity.this, MainActivity.class);
                startActivity(cerrarSesion);
                try {
                    ContactoActivity.super.onDestroy();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        });
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        switch (requestCode){
            case PHONE_CALL_CODE:
                String permiso = permissions[0];
                int result = grantResults[0];

                if (permiso.equals(Manifest.permission.CALL_PHONE)){
                    if (result == PackageManager.PERMISSION_GRANTED){
                        String telNumero = tVTelefono.getText().toString();
                        Intent llamarNum = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + telNumero));
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) return;
                            startActivity(llamarNum);
                    } else {
                        Toast.makeText(this, "Necesitas aceptar el permiso", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

    private boolean revPermisos (String permiso){
        int result = this.checkCallingOrSelfPermission(permiso);
        return result == PackageManager.PERMISSION_GRANTED;
    }
}