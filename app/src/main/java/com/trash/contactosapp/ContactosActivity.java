package com.trash.contactosapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class ContactosActivity extends AppCompatActivity {

    ListView lvContactos;
    CardView btnSalir;
    ArrayList<Contacto> contactos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactos);

        lvContactos = (ListView) findViewById(R.id.lvContactos);
        btnSalir = (CardView) findViewById(R.id.btnLogOut);

        contactos = new ArrayList<>();
        contactos.add(new Contacto("Carlos", "Lucas", "5610183216","upt.lucas@outlook.com"));
        contactos.add(new Contacto("Bernabe", "Agosto", "5572087295","lucas97@gmail.com"));

        ArrayList<String> nombreContactos = new ArrayList<>();
        for (Contacto contacto: contactos) {
            nombreContactos.add(contacto.getNombre() + " " +contacto.getApellido());
        }
        lvContactos.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, nombreContactos));

        lvContactos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(ContactosActivity.this, ContactoActivity.class);
                i.putExtra("NOMBRECONTACTO", contactos.get(position).getNombre());
                i.putExtra("APELLIDOCONTACTO", contactos.get(position).getApellido());
                i.putExtra("TELEFONOCONTACTO", contactos.get(position).getTelefono());
                i.putExtra("EMAILCONTACTO", contactos.get(position).getDireccion());
                startActivity(i);
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cerrarSesion = new Intent(ContactosActivity.this, MainActivity.class);
                startActivity(cerrarSesion);
                try {
                    ContactosActivity.super.onDestroy();
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }
        });
    }
}