package com.trash.contactosapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText txtUser, txtPass;
    CardView btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainActivity();
    }

    void MainActivity(){
        txtUser = (EditText) findViewById(R.id.userID);
        txtPass = (EditText) findViewById(R.id.Password);
        btnLogin = (CardView) findViewById(R.id.Login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                revUsuario();
            }
        });
    }

    boolean isEmpty(EditText text){
        CharSequence strg = text.getText().toString();
        return TextUtils.isEmpty(strg);
    }

    void revUsuario(){
        boolean esValido = true;

        if (isEmpty(txtUser)){
            txtUser.setError("Ingresa un nombre de usuario.");
            esValido = false;
        }
        if (isEmpty(txtPass)){
            txtPass.setError("Ingresa una contraseña.");
            esValido = false;
        } else {
            if (txtPass.getText().toString().length() < 4){
                txtPass.setError("La contraseña debe tener mas de 4 caractéres");
                esValido = false;
            }
        }

        if (esValido){
            String usuarioVal = txtUser.getText().toString();
            String passVal = txtPass.getText().toString();

            if (usuarioVal.equals("admin") && passVal.equals("pass123")){
                Intent iniciarSesion = new Intent(MainActivity.this, ContactosActivity.class);
                startActivity(iniciarSesion);
                this.finish();
            } else {
                Toast msg = Toast.makeText(this, "Usuario o contraseña incorrectos", Toast.LENGTH_LONG);
                msg.show();
            }
        }
    }
}